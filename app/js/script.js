$(document).ready(function () {
    /*-------------------------------------------- dd-menu ------------------------*/
    $(document).on('click', '.button-toggle-navigation', function () {
        $(this).toggleClass('isActive');
        $('.navigation-list').slideToggle();
        return false;
    });

    $(document).on('click', function (e) {
        if (jQuery('.navigation-list').css('display') === 'block') {
            $('.button-toggle-navigation').toggleClass('isActive');
            $('.navigation-list').slideToggle();
        }
    });

    /*--------------------------------------------Progress Bar and scroll with Fade in------------------------*/
    var circles = [
        {
            id : '.months',
            props: {
                startAngle: -Math.PI / 4 * 4,
                thickness: 7,
                value: 0.35,
                fill: {color: ['rgb(21, 43, 60)']},
                emptyFill: 'rgba(229, 227, 227, 1)',
                animation: {
                    duration: 3500,
                    easing: 'circleProgressEasing'
                }
            },
            isShow: false
        },
        {
            id : '.clients',
            props: {
                startAngle: -Math.PI / 4 * 6,
                thickness: 7,
                value: 0.5,
                fill: {color: ['rgb(0, 153, 137)']},
                emptyFill: 'rgba(229, 227, 227, 1)',
                animation: {
                    duration: 3500,
                    easing: 'circleProgressEasing'
                }
            },

            isShow: false
        },
        {
            id : '.projects',
            props: {
                startAngle: -Math.PI / 2 * 2,
                thickness: 7,
                value: 0.8,
                fill: {color: ['rgb(243, 71, 57)']},
                emptyFill: 'rgba(229, 227, 227, 1)',
                animation: {
                    duration: 3500,
                    easing: 'circleProgressEasing'
                }
            },
            isShow: false
        }
    ];

    $(window).on("load", function () {
        $(window).scroll(function () {
            var windowBottom = $(this).scrollTop() + $(this).innerHeight();

            circles.forEach(function (item) {
                var objectBottom = $(item.id).offset().top + $(item.id).outerHeight();

                /* If the element is completely within bounds of the window, fade it in */
                if (objectBottom < windowBottom && !item.isShow) { //object comes into view (scrolling down)
                    $(item.id).fadeTo(500, 1);
                    $(item.id).circleProgress(item.props).on('circle-animation-progress', function (event, progress, stepValue) {
                        $(this).find('strong').text(String(stepValue.toFixed(2)).substr(2));
                    });
                    item.isShow = true;
                }
            });
        }).scroll(); //invoke scroll-handler on page-load
    });


    $('.porrtfolio-img-wrapper').hover(function () {
        $(this).find('.learn-more-wrapper').fadeToggle(300)
    });
});